-- local baseUrl = "https://raw.githubusercontent.com/Ampix/BuildersScripts/master/"
local baseUrl = "https://gitlab.com/HVCsano/BuildersScripts/raw/main/"

local function checkDir()
    if not fs.exists("ampix/basaltWeb.lua") then
        term.setCursorPos(1,2)
        write("Basalt nem található: wget run https://basalt.madefor.cc/install.lua web")
        
    elseif fs.exists("ampix") and fs.isDir("ampix") then
        fs.delete("ampix")
        fs.makeDir("ampix")
        local verlua = io.open("ampix/updater.lua","w+")
        local file = http.get(baseUrl .. "updater.lua")
        if file then
            local content = file.readAll()
            file.close()
            verlua:write(content)
            verlua:close()
            shell.execute("ampix/updater")
        end
    else
        fs.makeDir("ampix")
        checkDir()
    end
end


term.clear()
term.setCursorPos(1,1)
write("Ampix Builders Scripts Loader")
sleep(0.5)
checkDir()