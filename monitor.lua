local ver = "0.2.0"

local basalt  = require("basaltWeb")

local function main()
    local main = basalt.createFrame()

    local button = main:addButton() --> Here we add our first button
    button:setPosition(4, 4) -- We want to change the default position of our button
    button:setSize(16, 3) -- And the default size.
    button:setText("Click me!") --> This method sets the text displayed on our button

    local function buttonClick() --> Create a function we want to call when the button gets clicked 
        basalt.debug("I got clicked!")
    end

    -- Now we just need to register the function to the button's onClick event handlers, this is how we can achieve that:
    button:onClick(buttonClick)
end

basalt.autoUpdate()
return {main=main}