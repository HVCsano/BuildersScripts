local baseUrl = "https://gitlab.com/HVCsano/BuildersScripts/raw/main/"

local updater = "0.2.15"

local programs = {
    "monitor"
}

term.clear()

for key,value in pairs(programs) do
    local verlua = io.open("ampix/" .. value .. ".lua","w+")
    local file = http.get(baseUrl .. value .. ".lua")
    if file then
        local content = file.readAll()
        file.close()
        verlua:write(content)
        verlua:close()
    end
end

local monitor = require("monitor")

term.clear()
term.setCursorPos(1,1)
write("Ampix Builders Scripts Updater v" .. updater)
term.setCursorPos(1,2)
sleep(1)
parallel.waitForAll(monitor.main())

return {baseUrl=baseUrl}